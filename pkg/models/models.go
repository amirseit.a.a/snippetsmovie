package models

import (
	"errors"
	"time"
)

var (
	ErrNoRecord = errors.New("models: no matching record found")
	ErrInvalidCredentials = errors.New("models: invalid credentials")
	ErrDuplicateEmail = errors.New("models: duplicate email")
)

type Movies struct {
	ID int
	Title string
	Description string
	Date time.Time
	Country string
	DateString string
}

type User struct {
	ID int
	Name string
	Email string
	HashedPassword []byte
	Created time.Time
	Active bool
}


