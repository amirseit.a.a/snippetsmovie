package pg

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/amirseit.a.a/movie/pkg/models"
	"time"
)

const (
	insertSql                 = "INSERT INTO movies (title,description,date,country) VALUES ($1,$2,$3,$4) RETURNING id"
	getMovieById            = "SELECT id, title,description,date,country FROM movies where id=$1"
	getAllMovies = "SELECT id, title,description,date,country country FROM movies"
	updateMovie = "UPDATE movies SET title = $2,description = $3,date = $4,country = $5 WHERE id = $1"
	deleteMovie = "DELETE FROM movies WHERE id = $1"

)

type MovieModel struct {
	Pool *pgxpool.Pool
}

func (m *MovieModel) Insert(title string,description string,date string,country string) (int, error) {
	var id uint64
	row := m.Pool.QueryRow(context.Background(), insertSql, title,description,date,country)
	err := row.Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (m *MovieModel) Get(id int) (*models.Movies, error) {
	s := &models.Movies{}
	err := m.Pool.QueryRow(context.Background(), getMovieById, id).
		Scan(&s.ID, &s.Title, &s.Description, &s.Date,&s.Country)
	if err != nil {
		fmt.Println(err.Error())
		if err.Error() == "no rows in result set" {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return s, nil
}

func (m *MovieModel) All() ([]*models.Movies, error) {
	movies := []*models.Movies{}
	rows, err := m.Pool.Query(context.Background(), getAllMovies)
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		s := &models.Movies{}
		err = rows.Scan(&s.ID, &s.Title, &s.Description, &s.Date,&s.Country)
		s.DateString = s.Date.Format("02/01/2006")
		if err != nil {
			return nil, err
		}
		movies = append(movies, s)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return movies, nil
}

func (m *MovieModel) Update(id int,title string,description string,date time.Time,country string)error  {
	_,err := m.Pool.Exec(context.Background(), updateMovie, id,title,description,date,country)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return  models.ErrInvalidCredentials
		} else {
			return  err
		}
	}
	return  nil
}

func (m *MovieModel) Delete(id int)error  {
	_,err := m.Pool.Exec(context.Background(), deleteMovie, id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return  models.ErrInvalidCredentials
		} else {
			return  err
		}
	}
	return  nil
}