package main

import (
	"errors"
	"fmt"
	"gitlab.com/amirseit.a.a/movie/pkg/forms"
	"gitlab.com/amirseit.a.a/movie/pkg/models"
	"net/http"
	"strconv"
	"time"
)



func (app *application) signupUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "signup.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}

func (app *application) signupUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		fmt.Println("here1")
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("name", "email", "password")
	form.MaxLength("name", 255)
	form.MaxLength("email", 255)
	form.MatchesPattern("email", forms.EmailRX)
	form.MinLength("password", 10)

	if !form.Valid() {
		app.render(w, r, "signup.page.tmpl", &templateData{Form: form})
		return
	}

	err = app.users.Insert(form.Get("name"), form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			form.Errors.Add("email", "Address is already in use")
			app.render(w, r, "signup.page.tmpl", &templateData{Form: form})
		} else {
			fmt.Println("here1",err.Error())
			app.serverError(w, err)
		}
		app.serverError(w, err)
		return
	}
	app.session.Put(r, "flash", "Your signup was successful. Please log in.")


	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
}

func (app *application) loginUserForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "login.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}
func (app *application) loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}
	form := forms.New(r.PostForm)

	id, err := app.users.Authenticate(form.Get("email"), form.Get("password"))
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.Errors.Add("generic", "Email or Password is incorrect")
			app.render(w, r, "login.page.tmpl", &templateData{Form: form})
		} else {
			app.serverError(w, err)
		}
		return
	}

	app.session.Put(r, "authenticatedUserID", id)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (app *application) logoutUser(w http.ResponseWriter, r *http.Request) {
	app.session.Remove(r, "authenticatedUserID")
	app.session.Put(r, "flash", "You've been logged out successfully!")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}



func (app *application) home(w http.ResponseWriter, r *http.Request) {
	s, err := app.movies.All()
	if err != nil {
		app.serverError(w, err)
		return
	}
	app.render(w, r, "home.page.tmpl", &templateData{
		Movies: s,
	})
}

func (app *application) showMovie(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get(":id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	s, err := app.movies.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}
	s.DateString = s.Date.Format("02/01/2006")
	flash := app.session.PopString(r, "flash")
	app.render(w, r, "show.page.tmpl", &templateData{
		//CSRFToken: nosurf.Token(r),
		Flash: flash,
		Movie: s,
		Form: forms.New(nil),
	})
}

func (app *application) createMovieForm(w http.ResponseWriter, r *http.Request) {
	app.render(w, r, "create.page.tmpl", &templateData{
		Form: forms.New(nil),
	})
}

func (app *application) createMovie(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("title", "description", "date", "country")
	form.MaxLength("title", 100)
	form.MaxLength("country", 100)

	if err != nil {
		fmt.Println(err)
	}


	if !form.Valid() {
		app.render(w, r, "create.page.tmpl", &templateData{Form: form})
		return
	}

	title := form.Get("title")
	description := form.Get("description")
	country := form.Get("country")
	date := form.Get("date")
	if err != nil {
		app.serverError(w, err)
		return
	}

	id, err := app.movies.Insert(title, description, date, country)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Movie successfully created!")
	http.Redirect(w, r, fmt.Sprintf("/movie/%d", id), http.StatusSeeOther)
}



func (app *application) updateMovieForm(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get(":id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	s, err := app.movies.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}
	s.DateString = s.Date.Format("2006-20-02")
	flash := app.session.PopString(r, "flash")
	app.render(w, r, "update.page.tmpl", &templateData{
		Flash: flash,
		Movie: s,
		Form: forms.New(nil),
	})
}
func (app *application) updateMovie(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get(":id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}
	fmt.Println(id)

	err = r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := forms.New(r.PostForm)
	form.Required("title", "description", "date", "country")
	form.MaxLength("title", 100)
	form.MaxLength("country", 100)

	if !form.Valid() {
		s, err := app.movies.Get(id)
		if err != nil {
			if errors.Is(err, models.ErrNoRecord) {
				app.notFound(w)
			} else {
				app.serverError(w, err)
			}
		}
		flash := app.session.PopString(r, "flash")
		app.render(w, r, "update.page.tmpl", &templateData{
			Flash: flash,
			Movie: s,
			Form: form,
		})
		return
	}

	title := form.Get("title")
	description := form.Get("description")
	country := form.Get("country")
	date, err := time.Parse("2006-1-02", form.Get("date"))
	if err != nil {
		app.serverError(w, err)
		return
	}

	err = app.movies.Update(id,title,description,date,country)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Movie successfully updated!")
	http.Redirect(w, r, fmt.Sprintf("/movie/%d", id), http.StatusSeeOther)
}

func (app *application) delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get(":id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	err = app.movies.Delete(id)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.session.Put(r, "flash", "Movie successfully deleted!")
	http.Redirect(w, r, fmt.Sprintf("/"), http.StatusSeeOther)
}
