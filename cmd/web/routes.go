package main

import (
	"github.com/bmizerany/pat"
	"github.com/justinas/alice"
	"net/http"
)


func (app *application) routes() http.Handler{
	standardMiddleware := alice.New(app.recoverPanic, app.logRequest, secureHeaders)
	dynamicMiddleware := alice.New(app.session.Enable, noSurf)

	mux := pat.New()
	mux.Get("/", dynamicMiddleware.ThenFunc(app.home))
	mux.Get("/movie/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createMovieForm))
	mux.Post("/movie/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createMovie))
	mux.Get("/movie/:id", dynamicMiddleware.ThenFunc(app.showMovie))

	mux.Get("/movie/update/:id", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.updateMovieForm))
	mux.Post("/movie/update/:id", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.updateMovie))

	mux.Post("/movie/delete/:id", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.delete))

	// Add the five new routes.
	mux.Get("/user/signup", dynamicMiddleware.ThenFunc(app.signupUserForm))
	mux.Post("/user/signup", dynamicMiddleware.ThenFunc(app.signupUser))
	mux.Get("/user/login", dynamicMiddleware.ThenFunc(app.loginUserForm))
	mux.Post("/user/login", dynamicMiddleware.ThenFunc(app.loginUser))
	mux.Post("/user/logout", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.logoutUser))


	fileServer := http.FileServer(http.Dir("./ui/static/"))
	mux.Get("/static/", http.StripPrefix("/static", fileServer))
	
	return standardMiddleware.Then(mux)
}
